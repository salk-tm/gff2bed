Installation
============

With ``conda``
--------------

``gff2bed`` is available from `bioconda <https://anaconda.org/bioconda/gff2bed>`_, and can be installed with ``conda``

.. code-block:: zsh

   conda install -c bioconda gff2bed

With ``pip``
------------

``gff2bed`` is available from `PyPI <https://pypi.org/project/gff2bed/>`_, and can be installed with ``pip``

.. code-block:: zsh

   pip install gff2bed
