.. gff2bed documentation master file, created by
   sphinx-quickstart on Thu Dec 29 11:32:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

``gff2bed`` documentation
=========================

Overview
--------

.. automodule:: gff2bed

Source code
-----------

See source code at `https://gitlab.com/salk-tm/gff2bed <https://gitlab.com/salk-tm/gff2bed>`_.

.. toctree::

   installation
   tutorial
   gff3_types_tags
   reference



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
